﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HDY_20071.Startup))]
namespace HDY_20071
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
