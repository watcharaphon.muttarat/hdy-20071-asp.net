namespace HDY_20071.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Microsoft.AspNet.Identity.EntityFramework;
    using HDY_20071.Models;
    using Microsoft.Build.Framework.XamlTypes;
    
    using Category = HDY_20071.Models.Category;

    public partial class AppDb : IdentityDbContext<ApplicationUser>
    {

        public static AppDb Create()
        {
            return new AppDb();
        }
        public AppDb()
            : base("name=AppDb")
        {

        }

        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Products> Products { get; set; }
        public DbSet<Customers> Customers { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Shippers> Shippers { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
