﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HDY_20071.Data;
using HDY_20071.Models;
using Newtonsoft.Json;

namespace HDY_20071.Controllers
{
    public class OrdersController : Controller
    {
        private AppDb db = new AppDb();

        // GET: Orders
        public ActionResult Index()
        {
            var orders = db.Orders.Include(o => o.customers).Include(o => o.employees).Include(o => o.shippers);
            return View(orders.ToList());
        }

        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var order = db.Orders.Include(o => o.OrderItem).Include(o => o.customers).Include(o => o.employees).Include(o => o.shippers
            )
                .FirstOrDefault(o => o.OrderID == id);


            ViewBag.Products = new SelectList(db.Products, "ProductID", "ProductName", "-- Please select --");
            ViewBag.Products1 = new SelectList(db.Products, "ProductID", "UnitPrice", "-- Please select --");
            ViewBag.Product = JsonConvert.SerializeObject(db.Products.ToList());


            if (order == null) return HttpNotFound();

            return View(order);
        }
        [HttpGet]
        public ActionResult GetOrderDetail(int id)
        {

            var o = db.Orders.Find(id);

            if (o == null) return HttpNotFound();

            return PartialView(o);
        }
        [HttpPost]
        public ActionResult AddOrderDetail(int id, OrderDetail item)
        {
            var o = db.Orders.Find(id);
            if (o == null) return HttpNotFound();

            var p = db.Products.Find(item.ProductId);
            if (p == null) return HttpNotFound();

            OrderDetail itm = o.OrderItem.FirstOrDefault(r => r.Products == p);
            var od = new OrderDetail();

            if (itm != null)
            {
                od = itm;
                od.Quantity += item.Quantity;
                od.UnitPrice = p.UnitPrice;
            }
            else
            {
                od = new OrderDetail();
                od.Products = p;
                od.UnitPrice = p.UnitPrice;
                od.Quantity = item.Quantity;
            }



            o.OrderItem.Add(od);

            db.SaveChanges();

            var res = new
            {
                success = "success"
            };

            return Json(res);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CompanyName");
            ViewBag.EmployeeId = new SelectList(db.Employees, "EmployeeId", "LastName");
            ViewBag.Shipvia = new SelectList(db.Shippers, "Shipvia", "CompanyName");
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        [Route("{controller}/create/{customerId}")]
        //public ActionResult Create([Bind(Include = "OrderId,OrderDate,RequiredDate,ShippedDate,Freight,ShipName,ShipAddress,ShipCity,ShipRegion,ShipPostalCode,ShipCountry,CustomerId,EmployeeId,ShipperId")] Order order)
        public ActionResult Create(int id, Orders order)
        {
            if (ModelState.IsValid)
            {
                //db.Orders.Add(order);
                var c = db.Customers.Find(id);
                c.Orders.Add(order);
                db.SaveChanges();
                //return RedirectToAction("Index");
                return RedirectToAction(nameof(Details), new { id = order.OrderID });
            }
            return RedirectToAction(nameof(Index), "Customers");
            //ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CompanyName", order.CustomerId);
            //ViewBag.EmployeeId = new SelectList(db.Employees, "EmployeeId", "LastName", order.EmployeeId);
            //ViewBag.ShipperId = new SelectList(db.Shippers, "ShipperId", "CompanyName", order.ShipperId);
            //return View(order);
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Orders orders = db.Orders.Find(id);
            if (orders == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CompanyName", orders.CustomerId);
            ViewBag.EmployeeId = new SelectList(db.Employees, "EmployeeId", "LastName", orders.EmployeeId);
            ViewBag.Shipvia = new SelectList(db.Shippers, "Shipvia", "CompanyName", orders.Shipvia);
            return View(orders);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrderID,OrderDate,RequiredDate,ShippedDate,Freight,ShipName,ShipAddress,ShipCity,ShipRegion,ShipPostalCode,ShipCountry,CustomerId,EmployeeId,Shipvia")] Orders orders)
        {
            if (ModelState.IsValid)
            {
                db.Entry(orders).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CompanyName", orders.CustomerId);
            ViewBag.EmployeeId = new SelectList(db.Employees, "EmployeeId", "LastName", orders.EmployeeId);
            ViewBag.Shipvia = new SelectList(db.Shippers, "Shipvia", "CompanyName", orders.Shipvia);
            return View(orders);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Orders orders = db.Orders.Find(id);
            if (orders == null)
            {
                return HttpNotFound();
            }
            return View(orders);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Orders orders = db.Orders.Find(id);
            db.Orders.Remove(orders);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
