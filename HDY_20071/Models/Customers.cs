﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HDY_20071.Models
{
    [Table("Customers")]
    public class Customers
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Index]
        [Column("customers_id")]
        [Display(Name = "Customer Id")]
        public int CustomerId { get; set; }

        [Required]
        [Column("company_name")]
        [Display(Name = "Company Name")]
        [StringLength(255, ErrorMessage = "company name at least 2 charector", MinimumLength = 2)]
        public string CompanyName { get; set; }

        [Required]
        [Column("contact_name")]
        [Display(Name = "Contact Name")]
        [StringLength(255, ErrorMessage = "contact name at least 2 charector", MinimumLength = 2)]
        public string ContactName { get; set; }

        [Required]
        [Column("contact_title")]
        [Display(Name = "Contact Title")]
        [StringLength(255, ErrorMessage = "contact title at least 2 charector", MinimumLength = 2)]
        public string ContactTitle { get; set; }

        [Required]
        [Column("address")]
        [Display(Name = "Address")]
        [StringLength(255, ErrorMessage = "contact name at least 2 charector", MinimumLength = 2)]
        public string Address { get; set; }


        [Column("city")]
        [Display(Name = "City")]
        [StringLength(100, ErrorMessage = "city at least 2 charector", MinimumLength = 2)]
        public string City { get; set; }


        [Column("region")]
        [Display(Name = "Region")]
        [StringLength(50, ErrorMessage = "region at least 2 charector", MinimumLength = 2)]
        public string Region { get; set; }

        [Required]
        [Column("postal_code")]
        [Display(Name = "Postal Code")]
        [StringLength(50, ErrorMessage = "postal code at least 2 charector", MinimumLength = 2)]
        public string PostalCode { get; set; }

        [Required]
        [Column("country")]
        [Display(Name = "Country")]
        [StringLength(50, ErrorMessage = "country at least 2 charector", MinimumLength = 2)]
        public string Country { get; set; }


        [Column("phone")]
        [Display(Name = "Phone")]
        [StringLength(15, ErrorMessage = "phone at least 2 charector", MinimumLength = 2)]
        public string Phone { get; set; }

        [Column("fax")]
        [Display(Name = "Fax")]
        [StringLength(15, ErrorMessage = "fax at least 2 charector", MinimumLength = 2)]
        public string Fax { get; set; }

        public virtual ICollection<Orders> Orders { get; set; }
        public int OrderCount => (Orders != null) ? Orders.Count() : 0;

    }
}