﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HDY_20071.Models
{
    [Table("Suppliers")]
    public class Supplier
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Index]
        [Column("supplier_id")]
        [Display(Name = "Supplier ID")]
        public int SupplierID { get; set; }

        [Required]
        [Column("company_name")]
        [Display(Name = "Company Name")]
        [StringLength(255, ErrorMessage = "company name at least 2 charector", MinimumLength = 2)]
        public string CompanyName { get; set; }


        [Column("contact_name")]
        [Display(Name = "Contact Name")]
        [StringLength(255, ErrorMessage = "contact name at least 2 charector", MinimumLength = 2)]
        public string ContactName { get; set; }


        [Column("contact_title")]
        [Display(Name = "Contact Title")]
        [StringLength(255, ErrorMessage = "contact title at least 2 charector", MinimumLength = 2)]
        public string ContactTitle { get; set; }

        [Required]
        [Column("address")]
        [Display(Name = "Address")]
        [StringLength(255, ErrorMessage = "contact name at least 2 charector", MinimumLength = 2)]
        public string Address { get; set; }


        [Column("city")]
        [Display(Name = "City")]
        [StringLength(100, ErrorMessage = "city at least 2 charector", MinimumLength = 2)]
        public string City { get; set; }


        [Column("region")]
        [Display(Name = "Region")]
        [StringLength(50, ErrorMessage = "region at least 2 charector", MinimumLength = 2)]
        public string Region { get; set; }

        [Required]
        [Column("postal_code")]
        [Display(Name = "Postal Code")]
        [StringLength(50, ErrorMessage = "postal code at least 2 charector", MinimumLength = 2)]
        public string PostalCode { get; set; }


        [Column("country")]
        [Display(Name = "Country")]
        [StringLength(50, ErrorMessage = "country at least 2 charector", MinimumLength = 2)]
        public string Country { get; set; }


        [Column("phone")]
        [Display(Name = "Phone")]
        [StringLength(15, ErrorMessage = "phone at least 2 charector", MinimumLength = 2)]
        public string Phone { get; set; }


        [Column("fax")]
        [Display(Name = "Fax")]
        [StringLength(15, ErrorMessage = "fax at least 2 charector", MinimumLength = 2)]
        public string Fax { get; set; }


        [Column("homepage")]
        [Display(Name = "Homepage")]
        [StringLength(100, ErrorMessage = "homepage at least 2 charector", MinimumLength = 2)]
        public string Homepage { get; set; }

        [Display(Name = "สร้างโดย")]
        [StringLength(255, ErrorMessage = "createby must be at least 2 charector", MinimumLength = 2)]
        [Column("create_by")]
        public string CreateBy { get; set; }



        [Required]
        [Display(Name = "วันที่สร้าง")]
        [Column("create_date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreateDate { get; set; } = DateTime.Now;


    }
}