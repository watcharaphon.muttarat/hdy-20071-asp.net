﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HDY_20071.Models
{
    [Table("Shippers")]
    public class Shippers
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Index]
        //Colum คือ ชื่อ ฟิลด์ของ ตาราง
        [Column("shipper_id")]
        //Display คือ ชื่อที่ใช้แสดงข้างๆtext box
        [Display(Name = "Shipper Id")]
        //Public int.... คือ ตัวแปร เวลาเราจะเรียกใช้
        public int Shipvia { get; set; }

        [Required]
        [Column("company_name")]
        [Display(Name = "Company Name")]
        [StringLength(255, ErrorMessage = "company name at least 2 charector", MinimumLength = 2)]
        public string CompanyName { get; set; }

        [Required]
        [Column("phone")]
        [Display(Name = "Phone")]
        [StringLength(15, ErrorMessage = "phone at least 2 charector", MinimumLength = 2)]
        public string Phone { get; set; }
    }
}