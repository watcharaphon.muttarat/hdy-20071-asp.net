﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HDY_20071.Models
{
    [Table("Employees")]
    public class Employees
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Index]
        [Column("employee_id")]
        [Display(Name = "Employee Id")]
        public int EmployeeId { get; set; }

        [Required]
        [Column("last_name")]
        [Display(Name = "Last Name")]
        [StringLength(255, ErrorMessage = "last name at least 2 charector", MinimumLength = 2)]
        public string LastName { get; set; }

        [Required]
        [Column("first_name")]
        [Display(Name = "First Name")]
        [StringLength(255, ErrorMessage = "first name at least 2 charector", MinimumLength = 2)]
        public string FirstName { get; set; }


        [Column("title")]
        [Display(Name = "Title")]
        [StringLength(255, ErrorMessage = "title at least 2 charector", MinimumLength = 2)]
        public string Title { get; set; }


        [Column("title_of_courtesy")]
        [Display(Name = "Title Of Courtesy")]
        [StringLength(255, ErrorMessage = "title of courtesy at least 2 charector", MinimumLength = 2)]
        public string TitleOfCourtesy { get; set; }

        [Required]
        [Column("birth_date")]
        [Display(Name = "Birth_Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; } = DateTime.Now;

        [Required]
        [Column("hire_date")]
        [DataType(DataType.Date)]
        [Display(Name = "Hire_Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime HireDate { get; set; } = DateTime.Now;

        [Required]
        [Column("address")]
        [Display(Name = "Address")]
        [StringLength(255, ErrorMessage = "contact name at least 2 charector", MinimumLength = 2)]
        public string Address { get; set; }


        [Column("city")]
        [Display(Name = "City")]
        [StringLength(100, ErrorMessage = "city at least 2 charector", MinimumLength = 2)]
        public string City { get; set; }


        [Column("region")]
        [Display(Name = "Region")]
        [StringLength(50, ErrorMessage = "region at least 2 charector", MinimumLength = 2)]
        public string Region { get; set; }

        [Required]
        [Column("postal_code")]
        [Display(Name = "Postal Code")]
        [StringLength(50, ErrorMessage = "postal code at least 2 charector", MinimumLength = 2)]
        public string PostalCode { get; set; }


        [Column("country")]
        [Display(Name = "Country")]
        [StringLength(50, ErrorMessage = "country at least 2 charector", MinimumLength = 2)]
        public string Country { get; set; }


        [Column("home_phone")]
        [Display(Name = "Home Phone")]
        [StringLength(15, ErrorMessage = "home phone at least 2 charector", MinimumLength = 2)]
        public string HomePhone { get; set; }


        [Column("extension")]
        [Display(Name = "Extension")]
        [StringLength(50, ErrorMessage = "extension at least 2 charector", MinimumLength = 2)]
        public string Extension { get; set; }


        [Column("notes")]
        [Display(Name = "Notes")]
        [StringLength(100, ErrorMessage = "notes at least 2 charector", MinimumLength = 2)]
        public string Notes { get; set; }


        [Column("reports_to")]
        [Display(Name = "Reports To")]

        public int? ReportsTo { get; set; }

    }
}