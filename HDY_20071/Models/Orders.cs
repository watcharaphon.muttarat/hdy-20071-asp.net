﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HDY_20071.Models
{
    [Table("Orders")]
    public class Orders
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Index]
        [Column("order_id")]
        [Display(Name = "Order ID")]
        public int OrderID { get; set; }

        [Required]
        [Column("order_date")]
        [Display(Name = "Order Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [DataType(DataType.Date)]

        public DateTime OrderDate { get; set; } = DateTime.Now;

        [Required]
        [Column("required_date")]
        [Display(Name = "Required Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [DataType(DataType.Date)]

        public DateTime RequiredDate { get; set; } = DateTime.Now;

        [Required]
        [Column("shipped_date")]
        [Display(Name = "Shipped Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [DataType(DataType.Date)]

        public DateTime ShippedDate { get; set; } = DateTime.Now;



        [Required]
        [Column("freight")]
        [Display(Name = "Freight")]

        public int Freight { get; set; }

        [Required]
        [Column("ship_name")]
        [Display(Name = "Ship Name")]
        [StringLength(255, ErrorMessage = "ship name at least 2 charector", MinimumLength = 2)]
        public string ShipName { get; set; }

        [Required]
        [Column("ship_address")]
        [Display(Name = "Ship Address")]
        [StringLength(255, ErrorMessage = "ship address at least 2 charector", MinimumLength = 2)]
        public string ShipAddress { get; set; }

        [Required]
        [Column("ship_city")]
        [Display(Name = "Ship City")]
        [StringLength(255, ErrorMessage = "ship city at least 2 charector", MinimumLength = 2)]
        public string ShipCity { get; set; }

        [Required]
        [Column("ship_region")]
        [Display(Name = "Ship Region")]
        [StringLength(255, ErrorMessage = "ship region at least 2 charector", MinimumLength = 2)]
        public string ShipRegion { get; set; }

        [Required]
        [Column("ship_postal_code")]
        [Display(Name = "Ship Postal Code")]
        [StringLength(255, ErrorMessage = "ship region at least 2 charector", MinimumLength = 2)]
        public string ShipPostalCode { get; set; }

        [Required]
        [Column("ship_country")]
        [Display(Name = "Ship Country")]
        [StringLength(255, ErrorMessage = "ship country at least 2 charector", MinimumLength = 2)]
        public string ShipCountry { get; set; }

        [Column("customer_id")]
        public int CustomerId { get; set; }
        public Customers customers { get; set; }

        [Column("employee_id")]
        public int EmployeeId { get; set; }
        public Employees employees { get; set; }

        [Column("ship_via")]
        public int Shipvia { get; set; }
        public Shippers shippers { get; set; }

        [Required]
        public virtual ICollection<OrderDetail> OrderItem { get; set; }
        = new HashSet<OrderDetail>();

        public decimal SubTotal => OrderItem.Sum(x => x.Total);  //Total เชื่อมกับหน้า  Model OrderDetail แล้วเอาไปใช้หน้า view Order หน้า Detail
        public decimal VatAmount => Math.Round(SubTotal * 0.07m, 2, MidpointRounding.AwayFromZero);
        public decimal NetTotal => SubTotal + VatAmount;









    }
}