﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HDY_20071.Models
{
    [Table("Products")]
    public class Products
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Index]
        //Colum คือ ชื่อ ฟิลด์ของ ตาราง
        [Column("product_id")]
        //Display คือ ชื่อที่ใช้แสดงข้างๆtext box
        [Display(Name = "Product ID")]
        //Public int.... คือ ตัวแปร เวลาเราจะเรียกใช้
        public int ProductId { get; set; }

        [Required]
        [Column("product_name")]
        [Display(Name = "Product Name")]
        [StringLength(50, ErrorMessage = "product name at least 2 charector", MinimumLength = 2)]
        public string ProductName { get; set; }

        [Required]
        [Column("quantity_per_unit")]
        [Display(Name = "Quantity Per Unit")]

        public int QuantityPerUnit { get; set; }

        [Column("unit_price")]
        [Display(Name = "Unit Price")]
        [Required]
        //[Column(TypeName = "decimal(10,2)")]
        public decimal UnitPrice { get; set; }

        [Column("units_in_stock")]
        [Display(Name = "Units In Stock")]
        [Required]
        //[Column(TypeName = "decimal(10,2)")]
        public int UnitsInStock { get; set; }

        [Column("units_on_order")]
        [Display(Name = "Units On Order")]

        //[Column(TypeName = "decimal(10,2)")]
        public int UnitsOnOrder { get; set; }

        [Column("reoder_level")]
        [Display(Name = "Reoder Level")]

        //[Column(TypeName = "decimal(10,2)")]
        public int ReoderLevel { get; set; }

        [Column("discontinued")]
        [Display(Name = "Discontinued")]

        //[Column(TypeName = "decimal(10,2)")]
        public int Discontinued { get; set; }

        [Column("imgepath")]
        [Display(Name = "ลิ้งค์รูปภาพ")]
        public string ImgePath { get; set; }

        //public HttpPostedFileBase ImageFile { get; set; }

        [Display(Name = "สร้างโดย")]
        [StringLength(255, ErrorMessage = "createby must be at least 2 charector", MinimumLength = 2)]
        [Column("create_by")]
        public string CreateBy { get; set; }



        [Required]
        [Display(Name = "วันที่สร้าง")]
        [Column("create_date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreateDate { get; set; } = DateTime.Now;

        //relational(foreign key)
        [Column("category_id")]
        public int CategoryId { get; set; } //CategoryId =1
        public Category category { get; set; } //CategoryId =1 , CategoryName = ...

        [Column("supplier_id")]
        public int SupplierId { get; set; } //SupplierId =1
        public Supplier supplier { get; set; } //SupplierId =1 , SupplierName = ...


    }

}