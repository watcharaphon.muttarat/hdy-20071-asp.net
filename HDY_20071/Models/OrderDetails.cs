﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HDY_20071.Models
{
    [Table("Order_Detail")]
    public class OrderDetail
    {

        [Column("unit_price")]
        [Display(Name = "Unit Price")]
        [Required]
        //[Column(TypeName = "decimal(10,2)")]
        public decimal UnitPrice { get; set; }

        [Required]
        [Column("quantity")]
        [Display(Name = "Quantity")]

        public int Quantity { get; set; }


        [Column("discount")]
        [Display(Name = "Discount")]

        public int Discount { get; set; }

        public decimal Total => (decimal)Quantity * UnitPrice; //Total เอาไปเชื่อมกับ Model Orders


        //ที่ต้องใส่ key ในนี้เพราะว่า ตารางนี้ไม่มี key หลัก เป็นของตัวเอง แต่ถ้า ตารางไหนมี key หลัก เป็นของตัวเองไม่ต้องใส่
        [Key, Column("order_id", Order = 0)]
        public int OrderId { get; set; }
        //public Orders orders { get; set; }
        public virtual Orders Orders { get; set; }

        //ที่ต้องใส่ key ในนี้เพราะว่า ตารางนี้ไม่มี key หลัก เป็นของตัวเอง แต่ถ้า ตารางไหนมี key หลัก เป็นของตัวเองไม่ต้องใส่
        [Key, Column("product_id", Order = 1)]
        public int ProductId { get; set; }
        //public Products products { get; set; }

        public virtual Products Products { get; set; }

    }
}