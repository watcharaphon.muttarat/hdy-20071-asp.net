﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HDY_20071.Models
{
    [Table("Categories")]
    public class Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Index]
        [Column("category_id")]
        [Display(Name = "Category ID")]
        public int CategoryId { get; set; }

        [Required]
        [Column("category_name")]
        [Display(Name = "Category Name")]
        [StringLength(50, ErrorMessage = "Category name at least 2 charector", MinimumLength = 2)]
        public string CategoryName { get; set; }


        [Column("description")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "สร้างโดย")]
        [StringLength(255, ErrorMessage = "createby must be at least 2 charector", MinimumLength = 2)]
        [Column("create_by")]
        public string CreateBy { get; set; }



        [Required]
        [Display(Name = "วันที่สร้าง")]
        [Column("create_date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreateDate { get; set; } = DateTime.Now;


    }
}