namespace HDY_20071.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial07 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        customers_id = c.Int(nullable: false, identity: true),
                        company_name = c.String(nullable: false, maxLength: 255),
                        contact_name = c.String(nullable: false, maxLength: 255),
                        contact_title = c.String(nullable: false, maxLength: 255),
                        address = c.String(nullable: false, maxLength: 255),
                        city = c.String(maxLength: 100),
                        region = c.String(maxLength: 50),
                        postal_code = c.String(nullable: false, maxLength: 50),
                        country = c.String(nullable: false, maxLength: 50),
                        phone = c.String(maxLength: 15),
                        fax = c.String(maxLength: 15),
                    })
                .PrimaryKey(t => t.customers_id)
                .Index(t => t.customers_id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        order_id = c.Int(nullable: false, identity: true),
                        order_date = c.DateTime(nullable: false),
                        required_date = c.DateTime(nullable: false),
                        shipped_date = c.DateTime(nullable: false),
                        freight = c.Int(nullable: false),
                        ship_name = c.String(nullable: false, maxLength: 255),
                        ship_address = c.String(nullable: false, maxLength: 255),
                        ship_city = c.String(nullable: false, maxLength: 255),
                        ship_region = c.String(nullable: false, maxLength: 255),
                        ship_postal_code = c.String(nullable: false, maxLength: 255),
                        ship_country = c.String(nullable: false, maxLength: 255),
                        customer_id = c.Int(nullable: false),
                        employee_id = c.Int(nullable: false),
                        ship_via = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.order_id)
                .ForeignKey("dbo.Customers", t => t.customer_id, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.employee_id, cascadeDelete: true)
                .ForeignKey("dbo.Shippers", t => t.ship_via, cascadeDelete: true)
                .Index(t => t.order_id)
                .Index(t => t.customer_id)
                .Index(t => t.employee_id)
                .Index(t => t.ship_via);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        employee_id = c.Int(nullable: false, identity: true),
                        last_name = c.String(nullable: false, maxLength: 255),
                        first_name = c.String(nullable: false, maxLength: 255),
                        title = c.String(maxLength: 255),
                        title_of_courtesy = c.String(maxLength: 255),
                        birth_date = c.DateTime(nullable: false),
                        hire_date = c.DateTime(nullable: false),
                        address = c.String(nullable: false, maxLength: 255),
                        city = c.String(maxLength: 100),
                        region = c.String(maxLength: 50),
                        postal_code = c.String(nullable: false, maxLength: 50),
                        country = c.String(maxLength: 50),
                        home_phone = c.String(maxLength: 15),
                        extension = c.String(maxLength: 50),
                        notes = c.String(maxLength: 100),
                        reports_to = c.Int(),
                    })
                .PrimaryKey(t => t.employee_id)
                .Index(t => t.employee_id);
            
            CreateTable(
                "dbo.Order_Detail",
                c => new
                    {
                        order_id = c.Int(nullable: false),
                        product_id = c.Int(nullable: false),
                        unit_price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        quantity = c.Int(nullable: false),
                        discount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.order_id, t.product_id })
                .ForeignKey("dbo.Orders", t => t.order_id, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.product_id, cascadeDelete: true)
                .Index(t => t.order_id)
                .Index(t => t.product_id);
            
            AddColumn("dbo.Suppliers", "fax", c => c.String(maxLength: 15));
            AddColumn("dbo.Suppliers", "homepage", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "ship_via", "dbo.Shippers");
            DropForeignKey("dbo.Order_Detail", "product_id", "dbo.Products");
            DropForeignKey("dbo.Order_Detail", "order_id", "dbo.Orders");
            DropForeignKey("dbo.Orders", "employee_id", "dbo.Employees");
            DropForeignKey("dbo.Orders", "customer_id", "dbo.Customers");
            DropIndex("dbo.Order_Detail", new[] { "product_id" });
            DropIndex("dbo.Order_Detail", new[] { "order_id" });
            DropIndex("dbo.Employees", new[] { "employee_id" });
            DropIndex("dbo.Orders", new[] { "ship_via" });
            DropIndex("dbo.Orders", new[] { "employee_id" });
            DropIndex("dbo.Orders", new[] { "customer_id" });
            DropIndex("dbo.Orders", new[] { "order_id" });
            DropIndex("dbo.Customers", new[] { "customers_id" });
            DropColumn("dbo.Suppliers", "homepage");
            DropColumn("dbo.Suppliers", "fax");
            DropTable("dbo.Order_Detail");
            DropTable("dbo.Employees");
            DropTable("dbo.Orders");
            DropTable("dbo.Customers");
        }
    }
}
