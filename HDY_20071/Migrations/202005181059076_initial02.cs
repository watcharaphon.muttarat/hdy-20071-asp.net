namespace HDY_20071.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial02 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        product_id = c.Int(nullable: false, identity: true),
                        product_name = c.String(nullable: false, maxLength: 50),
                        quantity_per_unit = c.Int(nullable: false),
                        unit_price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.product_id)
                .Index(t => t.product_id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Products", new[] { "product_id" });
            DropTable("dbo.Products");
        }
    }
}
