namespace HDY_20071.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial03 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Shippers",
                c => new
                    {
                        shipper_id = c.Int(nullable: false, identity: true),
                        company_name = c.String(nullable: false, maxLength: 255),
                        phone = c.String(nullable: false, maxLength: 15),
                    })
                .PrimaryKey(t => t.shipper_id)
                .Index(t => t.shipper_id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Shippers", new[] { "shipper_id" });
            DropTable("dbo.Shippers");
        }
    }
}
