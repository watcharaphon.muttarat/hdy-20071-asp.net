namespace HDY_20071.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial12 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "create_by", c => c.String(maxLength: 255));
            AddColumn("dbo.Suppliers", "create_date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "create_date");
            DropColumn("dbo.Suppliers", "create_by");
        }
    }
}
