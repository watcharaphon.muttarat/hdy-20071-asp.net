namespace HDY_20071.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial09 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "imgepath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "imgepath");
        }
    }
}
