namespace HDY_20071.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial05 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        category_id = c.Int(nullable: false, identity: true),
                        category_name = c.String(nullable: false, maxLength: 50),
                        description = c.String(),
                    })
                .PrimaryKey(t => t.category_id)
                .Index(t => t.category_id);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        supplier_id = c.Int(nullable: false, identity: true),
                        company_name = c.String(nullable: false, maxLength: 255),
                        contact_name = c.String(maxLength: 255),
                        contact_title = c.String(maxLength: 255),
                        address = c.String(nullable: false, maxLength: 255),
                        city = c.String(maxLength: 100),
                        region = c.String(maxLength: 50),
                        postal_code = c.String(nullable: false, maxLength: 50),
                        country = c.String(maxLength: 50),
                        phone = c.String(maxLength: 15),
                    })
                .PrimaryKey(t => t.supplier_id)
                .Index(t => t.supplier_id);
            
            AddColumn("dbo.Products", "category_id", c => c.Int(nullable: false));
            AddColumn("dbo.Products", "supplier_id", c => c.Int(nullable: false));
            CreateIndex("dbo.Products", "category_id");
            CreateIndex("dbo.Products", "supplier_id");
            AddForeignKey("dbo.Products", "category_id", "dbo.Categories", "category_id", cascadeDelete: true);
            AddForeignKey("dbo.Products", "supplier_id", "dbo.Suppliers", "supplier_id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "supplier_id", "dbo.Suppliers");
            DropForeignKey("dbo.Products", "category_id", "dbo.Categories");
            DropIndex("dbo.Suppliers", new[] { "supplier_id" });
            DropIndex("dbo.Categories", new[] { "category_id" });
            DropIndex("dbo.Products", new[] { "supplier_id" });
            DropIndex("dbo.Products", new[] { "category_id" });
            DropColumn("dbo.Products", "supplier_id");
            DropColumn("dbo.Products", "category_id");
            DropTable("dbo.Suppliers");
            DropTable("dbo.Categories");
        }
    }
}
