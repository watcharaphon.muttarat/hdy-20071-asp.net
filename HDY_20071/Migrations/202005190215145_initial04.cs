namespace HDY_20071.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial04 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "units_in_stock", c => c.Int(nullable: false));
            AddColumn("dbo.Products", "units_on_order", c => c.Int(nullable: false));
            AddColumn("dbo.Products", "reoder_level", c => c.Int(nullable: false));
            AddColumn("dbo.Products", "discontinued", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "discontinued");
            DropColumn("dbo.Products", "reoder_level");
            DropColumn("dbo.Products", "units_on_order");
            DropColumn("dbo.Products", "units_in_stock");
        }
    }
}
