namespace HDY_20071.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "create_by", c => c.String(maxLength: 255));
            AddColumn("dbo.Products", "create_date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "create_date");
            DropColumn("dbo.Products", "create_by");
        }
    }
}
